﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BuyNow
{
	/// <summary>
	/// Interaction logic for AppWindowMainPage.xaml
	/// </summary>
	public partial class AppWindowMainPage : Page
	{
		string username;

		public AppWindowMainPage()
		{
			InitializeComponent();
		}

		public AppWindowMainPage(string _Username)
		{
			InitializeComponent();

			username = _Username;
		}

		private void SearchProduct()
		{
			
			this.NavigationService.Navigate(new SearchResults(SearchField.Text, username));
		}

		private void LogoutButton_Click(object sender, EventArgs e)
		{
			MainWindow window = new MainWindow();
			window.Show();
			Window.GetWindow(this).Close();
		}

		private void SearchFieldKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
			if (e.Key == System.Windows.Input.Key.Enter)
			{
				if (SearchField.Text.Length != 0)
				{
					SearchProduct();
				}
			}
		}

		private void SearchFieldButton(object sender, RoutedEventArgs e)
		{
			if (SearchField.Text.Length != 0)
			{
				SearchProduct();
			}
		}

		private void Fav_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(new SearchResults(username));
		}
	}
}
