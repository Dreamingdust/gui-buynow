﻿using System;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using CsvHelper;

namespace BuyNow
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class RegLoginForm : Page
	{
		TextBlock InputError = new TextBlock();
		static CsvWriter csvWriter;
		static CsvReader csvReader;
		static StreamWriter streamWriter;
		static StreamReader streamReader;

		public RegLoginForm()
		{
			InitializeComponent();
			InitializeUserFiles();
		}

		public class UserCredentials
		{
			public string FirstName { get; set; }
			public string SecondName { get; set; }
			public string User { get; set; }
			public byte[] HashPass { get; set; }

			public UserCredentials()
			{
			}

			public UserCredentials(string FirstName, string SecondName, string User, byte[] HashPass)
			{
				this.FirstName = FirstName;
				this.SecondName = SecondName;
				this.User = User;
				this.HashPass = HashPass;
			}
		}

		/// <summary>
		/// Class taken from http://csharptest.net/470/another-example-of-how-to-store-a-salted-password-hash/
		/// Hashing algorithm with salt value for users' password storage.
		/// Includes a function to compare two hashes for login verification.
		/// </summary>
		public sealed class PasswordHash
		{
			const int SaltSize = 16, HashSize = 20, HashIter = 1000;
			readonly byte[] _salt, _hash;
			public PasswordHash(string password)
			{
				new RNGCryptoServiceProvider().GetBytes(_salt = new byte[SaltSize]);
				_hash = new Rfc2898DeriveBytes(password, _salt, HashIter).GetBytes(HashSize);
			}
			public PasswordHash(byte[] hashBytes)
			{
				Array.Copy(hashBytes, 0, _salt = new byte[SaltSize], 0, SaltSize);
				Array.Copy(hashBytes, SaltSize, _hash = new byte[HashSize], 0, HashSize);
			}
			public PasswordHash(byte[] salt, byte[] hash)
			{
				Array.Copy(salt, 0, _salt = new byte[SaltSize], 0, SaltSize);
				Array.Copy(hash, 0, _hash = new byte[HashSize], 0, HashSize);
			}
			public byte[] ToArray()
			{
				byte[] hashBytes = new byte[SaltSize + HashSize];
				Array.Copy(_salt, 0, hashBytes, 0, SaltSize);
				Array.Copy(_hash, 0, hashBytes, SaltSize, HashSize);
				return hashBytes;
			}
			public byte[] Salt { get { return (byte[])_salt.Clone(); } }
			public byte[] Hash { get { return (byte[])_hash.Clone(); } }
			public bool Verify(string password)
			{
				byte[] test = new Rfc2898DeriveBytes(password, _salt, HashIter).GetBytes(HashSize);
				for (int i = 0; i < HashSize; i++)
					if (test[i] != _hash[i])
						return false;
				return true;
			}
		}

		/// <summary>
		/// Initialization of the needed directory and files,
		/// if they are not already existing.
		/// </summary>
		public void InitializeUserFiles()
		{

			if (!Directory.Exists(@"../../Users/"))
			{
				Directory.CreateDirectory(@"../../Users/");
			}

			if (!File.Exists(@"../../Users/users.csv"))
			{
				File.Create(@"../../Users/users.csv").Close();
			}

			if (new FileInfo(@"../../Users/users.csv").Length == 0)
			{
				streamWriter = new StreamWriter(@"../../Users/users.csv");
				csvWriter = new CsvWriter(streamWriter);
				csvWriter.Configuration.Delimiter = ",";
				csvWriter.WriteHeader<UserCredentials>();
				csvWriter.NextRecord();
				streamWriter.Close();
			}
		}

		/// <summary>
		/// Handles the registration and login forms and is called by their event handlers.
		/// All verification is done through the handler in a responsive manner.
		/// </summary>
		/// <param name="source"></param>
		public void InputHandler(string source)
		{
			MainWindowGrid.Children.Remove(InputError);

			RegisterFirstName.BorderBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xab, 0xad, 0xb3));
			RegisterFamilyName.BorderBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xab, 0xad, 0xb3));
			RegisterUsername.BorderBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xab, 0xad, 0xb3));
			RegisterPassword.BorderBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xab, 0xad, 0xb3));
			Username.BorderBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xab, 0xad, 0xb3));
			Password.BorderBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xab, 0xad, 0xb3));

			InputError.Text = "";
			InputError.Width = 200;
			InputError.Height = 60;
			InputError.TextWrapping = TextWrapping.Wrap;
			InputError.Foreground = Brushes.Red;


			if (source == "Register")
			{
				Grid.SetColumn(InputError, 0);
				InputError.Margin = new Thickness(0, 205, 0, 0);

				bool emptyUserPass = false;

				if (string.IsNullOrEmpty(RegisterUsername.Text) && !RegisterPassword.IsFocused)
				{
					RegisterUsername.BorderBrush = Brushes.Red;
					InputError.Text = "Der Username darf nicht leer sein.";
					emptyUserPass = true;
				} else if (string.IsNullOrEmpty(RegisterPassword.Password) && !RegisterUsername.IsFocused)
				{
					RegisterPassword.BorderBrush = Brushes.Red;
					InputError.Text = "Das Passwort darf nicht leer sein.";
					emptyUserPass = true;
				}

				if (!emptyUserPass)
				{
					if (RegisterUsername.Text.Length > 12)
					{
						InputError.Text = "Der Username darf nicht mehr als 12 Zeichen sein.";
						RegisterUsername.BorderBrush = Brushes.Red;
					} else if (RegisterPassword.Password.Length > 12)
					{
						InputError.Text = "Das Passwort darf nicht mehr als 12 Zeichen sein.";
						RegisterPassword.BorderBrush = Brushes.Red;
					} else
					{
						var validateName = new Regex(@"^[a-zA-Z]*$");
						var validateUser = new Regex(@"^[a-zA-Z0-9_-]*$");
						var validatePass = new Regex(@"^[a-zA-Z0-9_\-.,#?!*()]*$");

						if (!validateName.IsMatch(RegisterFirstName.Text))
						{
							InputError.Text = "Der Vorname kann nur Buchstaben enthalten.";
							RegisterFirstName.BorderBrush = Brushes.Red;

						} else if (!validateName.IsMatch(RegisterFamilyName.Text))
						{
							InputError.Text = "Der Nachname kann nur Buchstaben enthalten.";
							RegisterFamilyName.BorderBrush = Brushes.Red;
						} else if (!validateUser.IsMatch(RegisterUsername.Text) && !RegisterPassword.IsFocused)
						{
							InputError.Text = "Der Username darf nur Buchstaben, Ziffern und _ oder - enthalten.";
							RegisterUsername.BorderBrush = Brushes.Red;
						} else if (!validatePass.IsMatch(RegisterPassword.Password) && !RegisterUsername.IsFocused)
						{
							InputError.Text = "Das Kennwort darf nur Buchstaben, Ziffern und _-.?!,#*() enthalten.";
							RegisterPassword.BorderBrush = Brushes.Red;
						} else
						{
							bool taken = false;

							streamReader = new StreamReader(@"../../Users/users.csv");
							csvReader = new CsvReader(streamReader);
							csvReader.Configuration.Delimiter = ",";
							System.Collections.Generic.List<UserCredentials> userRecords = csvReader.GetRecords<UserCredentials>().ToList();

							if (userRecords.Any())
							{
								foreach (UserCredentials user in userRecords)
								{
									if (user.User == RegisterUsername.Text)
									{
										RegisterUsername.BorderBrush = Brushes.Red;
										InputError.Text = "Der Username ist genommen, bitte wählen Sie anderen aus.";
										taken = true;
									}
								}
							}

							streamReader.Close();

							StackTrace trace = new StackTrace();
							if (!taken && trace.GetFrame(1).GetMethod().Name != "LostFocusHandler")
							{
								streamWriter = new StreamWriter(@"../../Users/users.csv", append: true);
								csvWriter = new CsvWriter(streamWriter);
								csvWriter.Configuration.Delimiter = ",";

								PasswordHash hash = new PasswordHash(RegisterPassword.Password);
								byte[] hashBytes = hash.ToArray();
								var UserRecord = new UserCredentials(RegisterFirstName.Text, RegisterFamilyName.Text, RegisterUsername.Text, hashBytes);

								csvWriter.WriteRecord(UserRecord);
								csvWriter.NextRecord();
								streamWriter.Flush();
								streamWriter.Close();
								InputError.Text = "";

								if (!Directory.Exists(@"../../Users/" + UserRecord.User.ToString()))
								{
									Directory.CreateDirectory(@"../../Users/" + UserRecord.User.ToString());
								}
								ApplicationWindow window = new ApplicationWindow(UserRecord.User.ToString());
								window.Show();
								Window.GetWindow(this).Close();
							}
						}
					}
				}
				try
				{
					MainWindowGrid.Children.Add(InputError);
				} catch (Exception)
				{
				}

			} else if (source == "Login")
			{
				if (string.IsNullOrEmpty(Username.Text) || string.IsNullOrEmpty(Password.Password))
				{
					if (string.IsNullOrEmpty(Username.Text) && !Password.IsFocused)
					{
						InputError.Text = "Kein Username eingegeben";
						Username.BorderBrush = Brushes.Red;
					} else if (string.IsNullOrEmpty(Password.Password) && !Username.IsFocused)
					{
						InputError.Text = "Kein Passwort eingegeben";
						Password.BorderBrush = Brushes.Red;
					}
				} else
				{
					UserCredentials currentUser = new UserCredentials();

					streamReader = new StreamReader(@"../../Users/users.csv");
					csvReader = new CsvReader(streamReader);
					csvReader.Configuration.Delimiter = ",";

					System.Collections.Generic.List<UserCredentials> userRecords = csvReader.GetRecords<UserCredentials>().ToList();

					if (userRecords.Any())
					{
						foreach (UserCredentials user in userRecords)
						{
							if (user.User == Username.Text)
							{
								currentUser = new UserCredentials(user.FirstName, user.SecondName, user.User, user.HashPass);
							}
						}
					}

					if (string.IsNullOrEmpty(currentUser.User))
					{
						InputError.Text = "Falsche Username oder Kennwort.";
						Username.BorderBrush = Brushes.Red;
					} else
					{
						PasswordHash hash = new PasswordHash(currentUser.HashPass);
						if (!hash.Verify(Password.Password))
						{
							InputError.Text = "Falsche Username oder Kennwort.";
						} else
						{
                            if (!Directory.Exists(@"../../Users/" + currentUser.User.ToString()))
                            {
                                Directory.CreateDirectory(@"../../Users/" + currentUser.User.ToString());
                            }
							ApplicationWindow window = new ApplicationWindow(currentUser.User.ToString());
							window.Show();
							Window.GetWindow(this).Close();
                        }
					}
					streamReader.Close();
				}
				Grid.SetColumn(InputError, 1);
				InputError.Margin = new Thickness(0, 155, 0, 0);
				MainWindowGrid.Children.Add(InputError);
			}
		}

		/// <summary>
		/// Handles for the XAML controls in the registration and login forms.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void RegisterHandler(object sender, RoutedEventArgs e)
		{
			InputHandler("Register");
		}

		private void LoginHandler(object sender, RoutedEventArgs e)
		{
			InputHandler("Login");
		}

		private void KeyDownHandler(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				InputHandler("Register");
			}
		}

		private void LoginKeyDownHandler(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				InputHandler("Login");
			}
		}

		private void LostFocusHandler(object sender, KeyboardFocusChangedEventArgs e)
		{
			InputHandler("Register");
		}
    }
}
