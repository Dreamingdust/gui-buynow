﻿using System;
using System.Windows;

namespace BuyNow
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			MainFrame.Navigate(new Uri("RegLoginForm.xaml", UriKind.Relative));

		}
	}
}
