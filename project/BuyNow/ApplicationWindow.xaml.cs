﻿using System;
using System.Windows;

namespace BuyNow
{
	/// <summary>
	/// Interaction logic for ApplicationWindow.xaml
	/// </summary>
	public partial class ApplicationWindow : Window
	{
		public ApplicationWindow()
		{
			InitializeComponent();

			MainFrame.Navigate(new Uri("AppWindowMainPage.xaml", UriKind.Relative));
		}

		public ApplicationWindow(string _User)
		{
			InitializeComponent();
			
			MainFrame.Navigate(new AppWindowMainPage(_User));
		}

		private void MinimizeButton_Click(object sender, RoutedEventArgs e)
		{
			App.Current.MainWindow.WindowState = WindowState.Minimized;
			foreach (Window win in Application.Current.Windows)
			{
				if (win.IsActive == true)
				{
					win.WindowState = WindowState.Minimized;
				}
			}
		}

		private void MaximizeButton_Click(object sender, RoutedEventArgs e)
		{
			foreach (Window win in Application.Current.Windows)
			{
				if (win.IsActive == true)
				{
					if (win.WindowState == WindowState.Maximized)
					{
						win.WindowState = WindowState.Normal;
						WindowSizeButton.Style = Resources["MaximizeButtonStyle"] as Style;
					} else if (win.WindowState == WindowState.Normal)
					{
						win.WindowState = WindowState.Maximized;
						WindowSizeButton.Style = Resources["RestoreButtonStyle"] as Style;
					}
				}
			}
		}

		private void CloseButton_Click(object sender, RoutedEventArgs e)
		{
			foreach (Window win in Application.Current.Windows)
			{
				if (win.IsActive == true)
				{
					win.Close();
					Application.Current.Shutdown();
				}
			}
		}

		private void DragWindow(object sender, RoutedEventArgs e)
		{
			foreach (Window win in Application.Current.Windows)
			{
				if (win.IsActive == true)
				{
					win.DragMove();
				}
			}
		}
	}
}
