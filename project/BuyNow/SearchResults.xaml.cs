﻿using System;
using System.Windows.Controls;
using System.Globalization;
using System.Net;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.Windows;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Windows.Documents;
using System.Windows.Navigation;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Newtonsoft.Json.Linq;
using CsvHelper;

namespace BuyNow
{

	/// <summary>
	/// Interaction logic for SearchResults.xaml
	/// </summary>
	public partial class SearchResults : Page
	{
		public static CultureInfo de_DE = new CultureInfo("de-DE");
		List<Product> products = new List<Product>();
		Dictionary<Product, string> pinned_products = new Dictionary<Product, string>();
		string username;

		class Product
		{
			public string Name { get; set; }
			public string Price { get; set; }
			public string Link { get; set; }
			public string ImageLink { get; set; }
			public string ShopName { get; set; }

			public Product()
			{
				Name = "";
				Price = "";
				Link = "";
				ImageLink = "";
				ShopName = "";
			}

			public Product(string name_input, string price_input, string link_input, string image_input, string shop_input)
			{
				Name = name_input;
				Price = price_input;
				Link = link_input;
				ImageLink = image_input;
				ShopName = shop_input;
			}
		}

		/// <summary>
		/// Dynamically loading of found results into the main grid of the application. 
		/// </summary>
		/// <param name="products"></param>
		/// <param name="_Source"></param>
		private void AddResultsToGrid(List<Product> products, string _Source)
		{
			foreach (Product item in products)
			{
				Product_Grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star), MaxHeight = 300 });

				Grid InformationGrid = new Grid();
				InformationGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(7, GridUnitType.Star) });
				InformationGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star), MaxHeight = 30 });

				Hyperlink hyperlink = new Hyperlink { NavigateUri = new Uri(item.Link), IsEnabled = true };
				hyperlink.Inlines.Add("Open in " + item.ShopName);
				hyperlink.RequestNavigate += new RequestNavigateEventHandler(delegate (object sender, RequestNavigateEventArgs e) {
					Process.Start(new ProcessStartInfo(e.Uri.ToString()));
					e.Handled = true;
				});

				TextBlock Hyperlink_Box = new TextBlock {
					FontSize = 14,
					Focusable = false,
					VerticalAlignment = VerticalAlignment.Bottom,
					Margin = new Thickness(0,0,0,5)
				};
				Hyperlink_Box.Inlines.Add(hyperlink);


				Image Product_Image = new Image();

				try
				{
					Product_Image = new Image { Source = new BitmapImage(new Uri(item.ImageLink)), MaxWidth = 200, MaxHeight = 200, Margin = new Thickness(3, 10, 3, 10) };
				} catch
				{
					Product_Image = new Image { Source = new BitmapImage(new Uri(@"../../Resources/image_not_found.png", UriKind.Relative)), MaxWidth = 200, MaxHeight = 200, Margin = new Thickness(3, 10, 3, 10) };
				}

				TextBlock Product_Window = new TextBlock {
					FontSize = 16,
					TextWrapping = TextWrapping.Wrap,
					Focusable = false,
					Text = item.Name + "\n\n" + "Preis: EUR " + item.Price + "\n\n",
					MinHeight = 150
				};

				Border image_bord = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 0, 1) };
				Border text_bord = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 0, 1) };

				Button pin = new Button { MinWidth = 30, MinHeight = 30, HorizontalAlignment = HorizontalAlignment.Right, Margin = new Thickness(0, 0, 35, 0) };

				pin.Background = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../Resources/pin.jpg", UriKind.Relative)) };
				pin.Click += new RoutedEventHandler(Pin_Click);

				Product_Grid.Children.Add(image_bord);
				Product_Grid.Children.Add(Product_Image);
				Product_Grid.Children.Add(InformationGrid);
				Product_Grid.Children.Add(text_bord);
				InformationGrid.Children.Add(Product_Window);
				InformationGrid.Children.Add(Hyperlink_Box);
				InformationGrid.Children.Add(pin);
				if (_Source == "search")
				{
					Button favourite = new Button { MinWidth = 30, MinHeight = 30, HorizontalAlignment = HorizontalAlignment.Right };
					favourite.Background = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../Resources/heart_fav.png", UriKind.Relative)) };
					favourite.Click += new RoutedEventHandler(Favourite_Click);
					InformationGrid.Children.Add(favourite);
					Grid.SetRow(favourite, 1);

					favourite.Name = "Search";
					pin.Name = "Search";
				} else if (_Source == "favourites")
				{
					Button unfavourite = new Button { MinWidth = 30, MinHeight = 30, HorizontalAlignment = HorizontalAlignment.Right };
					unfavourite.Background = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../Resources/heart_unfav.png", UriKind.Relative)) };
					unfavourite.Click += new RoutedEventHandler(Unfavourite_Click);
					unfavourite.Name = "List";
					InformationGrid.Children.Add(unfavourite);
					Grid.SetRow(unfavourite, 1);

					pin.Name = "Favourite";
				}

				Grid.SetRow(image_bord, Product_Grid.RowDefinitions.Count - 1);
				Grid.SetRow(Product_Image, Product_Grid.RowDefinitions.Count - 1);
				Grid.SetRow(InformationGrid, Product_Grid.RowDefinitions.Count - 1);
				Grid.SetRow(text_bord, Product_Grid.RowDefinitions.Count - 1);
				Grid.SetRow(Product_Window, 0);
				Grid.SetRow(Hyperlink_Box, 1);
				Grid.SetRow(pin, 1);
				Grid.SetColumn(image_bord, 0);
				Grid.SetColumn(Product_Image, 0);
				Grid.SetColumn(InformationGrid, 1);
				Grid.SetColumn(text_bord, 1);
			}
		}

		private void AddPinnedToGrid(Dictionary<Product, string> pinned_products)
		{
			foreach (KeyValuePair<Product, string> item in pinned_products)
			{
				Pin_Grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star), MaxHeight = 300 });

				Grid InformationGrid = new Grid();
				InformationGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
				InformationGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(4, GridUnitType.Star) });
				InformationGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

				Hyperlink hyperlink = new Hyperlink { NavigateUri = new Uri(item.Key.Link), IsEnabled = true };
				hyperlink.Inlines.Add(item.Key.ShopName);
				hyperlink.RequestNavigate += new RequestNavigateEventHandler(delegate (object s, RequestNavigateEventArgs eventArgs) {
					Process.Start(new ProcessStartInfo(eventArgs.Uri.ToString()));
					eventArgs.Handled = true;
				});

				TextBlock Hyperlink_Box = new TextBlock {
					FontSize = 13,
					Focusable = false,
					VerticalAlignment = VerticalAlignment.Bottom,
					HorizontalAlignment = HorizontalAlignment.Left,
					Margin = new Thickness(0, 0, 0, 5),
					MaxWidth = 150
				};
				Hyperlink_Box.Inlines.Add(hyperlink);

				TextBlock Product_Window = new TextBlock {
					FontSize = 14,
					TextWrapping = TextWrapping.Wrap,
					Focusable = false,
					Text = item.Key.Name + "\n\n" + "Preis: EUR " + item.Key.Price + "\n\n",
					MinHeight = 150
				};

				Border image_bord = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 0, 1) };
				Border text_bord = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 0, 1) };

				Button unpin = new Button { Width = 30, Height = 30, HorizontalAlignment = HorizontalAlignment.Right, Margin = new Thickness(0, 0, 35, 0) };

				unpin.Background = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../Resources/unpin.png", UriKind.Relative)) };
				unpin.Click += new RoutedEventHandler(Unpin_Click);

				if (!FavouriteCheck(item.Key))
				{ 
					Button favourite = new Button { Width = 30, Height = 30, HorizontalAlignment = HorizontalAlignment.Right };
					favourite.Background = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../Resources/heart_fav.png", UriKind.Relative)) };
					favourite.Click += new RoutedEventHandler(Favourite_Click);
					favourite.Name = "Pin";
					InformationGrid.Children.Add(favourite);
					Grid.SetRow(favourite, 2);

					unpin.Name = "Search";
				} else if (FavouriteCheck(item.Key))
				{
					Button unfavourite = new Button { Width = 30, Height= 30, HorizontalAlignment = HorizontalAlignment.Right };
					unfavourite.Background = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../Resources/heart_unfav.png", UriKind.Relative)) };
					unfavourite.Click += new RoutedEventHandler(Unfavourite_Click);
					unfavourite.Name = "Pin";
					InformationGrid.Children.Add(unfavourite);
					Grid.SetRow(unfavourite, 2);

					unpin.Name = "Favourite";
				}

				Pin_Grid.Children.Add(InformationGrid);
				InformationGrid.Children.Add(Product_Window);
				InformationGrid.Children.Add(unpin);
				InformationGrid.Children.Add(Hyperlink_Box);
				InformationGrid.Children.Add(text_bord);

				Grid.SetRow(InformationGrid, Pin_Grid.RowDefinitions.Count - 1);
				Grid.SetRow(text_bord, InformationGrid.RowDefinitions.Count - 1);
				Grid.SetRow(Product_Window, 1);
				Grid.SetRow(unpin, 2);
				Grid.SetRow(Hyperlink_Box, 2);
			}
		}

		/// <summary>
		/// Search query for scraping data of Amazon
		/// </summary>
		/// <param name="query"></param>
		void AmazonRead(string query)
		{
			query = query.Replace(" ", "+");
			string url = @"https://www.amazon.de/s/ref=nb_sb_noss_2?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&url=search-alias%3Daps&field-keywords=" + query;

			string content = "";

			try
			{
				content = new WebClient().DownloadString(url);
			} catch (Exception)
			{ }

			var HtmlDoc = new HtmlDocument();
			HtmlDoc.LoadHtml(content);

			if (HtmlDoc.DocumentNode.SelectSingleNode("//h1[@id='noResultsTitle']") == null)
			{
				for (int i = 0; i < 20; i++)
				{
					if (HtmlDoc.DocumentNode.SelectSingleNode("//li[@id='result_" + i + "']//h5") == null)
					{
						string ProductName = @"";
						string Link = @"";
						string ImageLink = @"";
						string Price = @"";

						try
						{
							if (HtmlDoc.DocumentNode.SelectSingleNode("//li[@id='result_" + i + "']//span[contains(@class, 's-price')]").InnerText == null)
							{
								Price = HtmlDoc.DocumentNode.SelectSingleNode("//li[@id='result_" + i + "']//span[contains(@class, 'a-color-price')]").InnerText;
							} else
							{
								Price = HtmlDoc.DocumentNode.SelectSingleNode("//li[@id='result_" + i + "']//span[contains(@class, 's-price')]").InnerText;
							}

							Price = Price.Replace("EUR ", String.Empty);
							if (Price.Contains("-"))
							{
								Price = Price.Substring(0, Price.IndexOf(" "));
							}

							if (!Price.Any(x => !char.IsLetter(x)))
							{
								Price = "0,00";
							}

							ProductName = HtmlDoc.DocumentNode.SelectSingleNode("//li[@id='result_" + i + "']//a[contains(@class, 's-access-detail-page')]").GetAttributeValue("title", "");
							Link = HtmlDoc.DocumentNode.SelectSingleNode("//li[@id='result_" + i + "']//a[contains(@class, 's-access-detail-page')]").GetAttributeValue("href", "");
							ImageLink = HtmlDoc.DocumentNode.SelectSingleNode("//li[@id='result_" + i + "']//img").GetAttributeValue("src", "");

							products.Add(new Product(ProductName, Price, Link, ImageLink, "Amazon"));

						} catch (Exception)
						{
						}
					}
				}
			}
		}

		/// <summary>
		/// Search query for scraping data of Otto
		/// </summary>
		/// <param name="query"></param>
		void OttoRead(string query)
		{
			query = query.Replace(" ", "%20");
			string url = @"https://www.otto.de/suche/" + query;
			string content = "";

			try
			{
				content = new WebClient().DownloadString(url);
			} catch (Exception)
			{}

			var HtmlDoc = new HtmlDocument();
			HtmlDoc.LoadHtml(content);

			if (HtmlDoc.DocumentNode.SelectSingleNode("//div[@id='san_searchResult']") != null)
			{
				for (int i = 1; i < 10; i++)
				{
					string ProductName = @"";
					string Link = @"";
					string ImageLink = @"";
					string Price = @"";

					var ArticleNode = HtmlDoc.DocumentNode.SelectSingleNode("//section[@id='san_resultSection']//article[@data-listposition='"+i+"']");
					try
					{
						ProductName = ArticleNode.SelectSingleNode("a").InnerHtml;
						System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex("[^a-zA-Z0-9 -]");
						ProductName = rgx.Replace(ProductName, "");
						ProductName = ProductName.TrimStart(' ');
						
						Link = @"http://www.otto.de" + ArticleNode.SelectSingleNode("a").GetAttributeValue("href", "");
						
						string json_script = ArticleNode.SelectSingleNode("script").InnerHtml;
						JObject jObject = JObject.Parse(json_script);

						Price = (string)jObject["product"]["variationMap"][ArticleNode.GetAttributeValue("data-facetted-variation-ids", "NotFound")]["formattedRetailPrice"];
						ImageLink = (string)jObject["product"]["variationMap"][ArticleNode.GetAttributeValue("data-facetted-variation-ids", "NotFound")]["imageUrl"];


						products.Add(new Product(ProductName, Price, Link, ImageLink, "Otto"));
					} catch (Exception)
					{
					}
				}
			}
		}

		public SearchResults()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Main constructor of the page. Calls the scraping funtions and then creates the product grid.
		/// </summary>
		/// <param name="_Search"></param>
		/// <param name="_Username"></param>
		public SearchResults(string _Search, string _Username)
		{
			InitializeComponent();
			username = _Username;
			AmazonRead(_Search);
			OttoRead(_Search);

			products = products.OrderBy(o => Convert.ToDouble(o.Price.Replace(".", string.Empty))).ToList();

			if (products.Count != 0)
			{
				AddResultsToGrid(products.OrderBy(o=> Convert.ToDouble(o.Price.Replace(".", string.Empty))).ToList(), "search");
			} else
			{
				Grid NoResults = new Grid();
				NoResults.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
				NoResults.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

				Product_Grid.Children.Add(NoResults);
				Grid.SetColumnSpan(NoResults, 2);
				
				Image NoResultsImage = new Image { Source = new BitmapImage(new Uri(@"../../Resources/no_results.png", UriKind.Relative)) };
				Grid.SetRow(NoResultsImage, 0);
				Grid.SetColumn(NoResults, 0);
				NoResults.Children.Add(NoResultsImage);
			}

			SearchField.Text = _Search;
		}

		/// <summary>
		/// Constructor for when the favourites are called from start page.
		/// </summary>
		/// <param name="_Username"></param>
		public SearchResults(string _Username)
		{
			InitializeComponent();
			username = _Username;

			ShowFavourites();
		}


		private void LogoutButton_Click(object sender, EventArgs e)
		{
			MainWindow window = new MainWindow();
			window.Show();
			Window.GetWindow(this).Close();
		}

		/// <summary>
		/// Button Events controlling most of the functionality.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SearchFieldKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
			if (e.Key == System.Windows.Input.Key.Enter)
			{
				if (SearchField.Text.Length != 0)
				{
					Product_Grid.Children.Clear();
					Product_Grid.RowDefinitions.Clear();
					products.Clear();
					AmazonRead(SearchField.Text);
					OttoRead(SearchField.Text);
					AddResultsToGrid(products.OrderBy(o => Convert.ToDouble(o.Price.Replace(".", string.Empty))).ToList(), "search");
				}
			}
		}

		private void SearchFieldButton(object sender, RoutedEventArgs e)
		{
			if (SearchField.Text.Length != 0)
			{
				Product_Grid.Children.Clear();
				Product_Grid.RowDefinitions.Clear();
				products.Clear();
				AmazonRead(SearchField.Text);
				OttoRead(SearchField.Text);
				AddResultsToGrid(products.OrderBy(o => Convert.ToDouble(o.Price.Replace(".", string.Empty))).ToList(), "search");
			}
		}

		private void Favourite_Click(object sender, RoutedEventArgs e)
		{
			StreamWriter streamWriter;
			StreamReader streamReader;
			CsvWriter csvWriter;
			CsvReader csvReader;

			if (!Directory.Exists(@"../../Users/" + username))
			{
				Directory.CreateDirectory(@"../../Users/" + username);
			}

			if (!File.Exists(@"../../Users/" + username + @"/favourites.csv"))
			{
				File.Create(@"../../Users/" + username + @"/favourites.csv").Close();
			}

			if (new FileInfo(@"../../Users/" + username + @"/favourites.csv").Length == 0)
			{
				streamWriter = new StreamWriter(@"../../Users/" + username + @"/favourites.csv");
				csvWriter = new CsvWriter(streamWriter);
				csvWriter.Configuration.Delimiter = ",";
				csvWriter.WriteHeader<Product>();
				csvWriter.NextRecord();
				streamWriter.Close();
			}

			Button src = e.Source as Button;
			products = products.OrderBy(o => Convert.ToDouble(o.Price.Replace(".", string.Empty))).ToList();

			streamReader = new StreamReader(@"../../Users/" + username + @"/favourites.csv");
			csvReader = new CsvReader(streamReader);
			csvReader.Configuration.Delimiter = ",";

			List<Product> userRecords = csvReader.GetRecords<Product>().ToList();
			bool alreadyFav = false;

			if (src.Name == "Pin")
			{
				foreach(var record in userRecords)
				{
					if (record.Name == pinned_products.Keys.ToList()[Grid.GetRow(src.Parent as Grid)].Name 
						&& record.ShopName == pinned_products.Keys.ToList()[Grid.GetRow(src.Parent as Grid)].ShopName
						&& record.Price == pinned_products.Keys.ToList()[Grid.GetRow(src.Parent as Grid)].Price)
					{
						alreadyFav = true;
					}
				}
			} else
			{
				foreach (var record in userRecords)
				{
					if (record.Name == products[Grid.GetRow(src.Parent as Grid)].Name
						&& record.ShopName == products[Grid.GetRow(src.Parent as Grid)].ShopName
						&& record.Price == products[Grid.GetRow(src.Parent as Grid)].Price)
					{
						alreadyFav = true;
					}
				}
			}

			streamReader.Close();
			
			if (!alreadyFav)
			{
				streamWriter = new StreamWriter(@"../../Users/" + username + @"/favourites.csv", append: true);
				csvWriter = new CsvWriter(streamWriter);
				csvWriter.Configuration.Delimiter = ",";

				if (src.Name == "Pin")
				{
					csvWriter.WriteRecord(pinned_products.Keys.ToList()[Grid.GetRow(src.Parent as Grid)]);
				} else
				{
					csvWriter.WriteRecord(products[Grid.GetRow(src.Parent as Grid)]);

					src.Name = "Unfavourite";
				}

				src.Background = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../Resources/heart_unfav.png", UriKind.Relative)) };
				src.Click -= Favourite_Click;
				src.Click += new RoutedEventHandler(Unfavourite_Click);

				csvWriter.NextRecord();
				streamWriter.Flush();
				streamWriter.Close();
			}
			
		}

		private void FavButton_Click(object sender, RoutedEventArgs e)
		{
			ShowFavourites();
		}

		private void ShowFavourites()
		{
			Product_Grid.Children.Clear();
			Product_Grid.RowDefinitions.Clear();

			if (File.Exists(@"../../Users/" + username + @"/favourites.csv"))
			{
				StreamReader streamReader = new StreamReader(@"../../Users/" + username + @"/favourites.csv");
				CsvReader csvReader = new CsvReader(streamReader);
				csvReader.Configuration.Delimiter = ",";
				List<Product> user_favourites = csvReader.GetRecords<Product>().ToList();
				streamReader.Close();


				AddResultsToGrid(user_favourites, "favourites");
			}
		}

		private void Unfavourite_Click(object sender, RoutedEventArgs e)
		{
			Button src = sender as Button;
			String FilePath = @"../../Users/" + username + @"/favourites.csv";

			StreamReader streamReader = new StreamReader(FilePath);
			CsvReader csvReader = new CsvReader(streamReader);
			csvReader.Configuration.Delimiter = ",";

			List<Product> user_favourites = csvReader.GetRecords<Product>().ToList();
			streamReader.Close();

			if (src.Name == "Pin")
			{
				File.WriteAllLines(FilePath, File.ReadLines(FilePath).Where(l => l.Contains(pinned_products.Keys.ToList()[Grid.GetRow(src.Parent as Grid)].Name) == false).ToList());

				src.Background = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../Resources/heart_fav.png", UriKind.Relative)) };
				src.Click -= Unfavourite_Click;
				src.Click += new RoutedEventHandler(Favourite_Click);
			} else if (src.Name == "List")
			{
				File.WriteAllLines(FilePath, File.ReadLines(FilePath).Where(l => l.Contains(user_favourites[Grid.GetRow(src.Parent as Grid)].Name) == false).ToList());
			} else if (src.Name == "Unfavourite")
			{
				File.WriteAllLines(FilePath, File.ReadLines(FilePath).Where(l => l.Contains(products[Grid.GetRow(src.Parent as Grid)].Name) == false).ToList());

				src.Background = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../Resources/heart_fav.png", UriKind.Relative)) };
				src.Click -= Unfavourite_Click;
				src.Click += new RoutedEventHandler(Favourite_Click);
				src.Name = "Search";
			}

			streamReader = new StreamReader(FilePath);
			csvReader = new CsvReader(streamReader);
			csvReader.Configuration.Delimiter = ",";

			user_favourites = csvReader.GetRecords<Product>().ToList();
			streamReader.Close();


			if (src.Name != "Search")
			{
				Product_Grid.Children.Clear();
				Product_Grid.RowDefinitions.Clear();
				AddResultsToGrid(user_favourites, "favourites");
			}
		}

		private void Pin_Click(object sender, RoutedEventArgs e)
		{
			Button src = sender as Button;

			if (src.Name == "Search")
			{
				if (!pinned_products.ContainsKey(products[Grid.GetRow(src.Parent as Grid)]))
				{
					pinned_products.Add(products[Grid.GetRow(src.Parent as Grid)], "Search");
				}
			} else if (src.Name == "Favourite")
			{
				StreamReader streamReader = new StreamReader(@"../../Users/" + username + @"/favourites.csv");
				CsvReader csvReader = new CsvReader(streamReader);
				csvReader.Configuration.Delimiter = ",";

				List<Product> user_favourites = csvReader.GetRecords<Product>().ToList();
				streamReader.Close();

				if (!pinned_products.ContainsKey(user_favourites[Grid.GetRow(src.Parent as Grid)]))	
				{
					pinned_products.Add(user_favourites[Grid.GetRow(src.Parent as Grid)], "Favourite");
				}
			}

			Pin_Grid.Children.Clear();
			Pin_Grid.RowDefinitions.Clear();
			AddPinnedToGrid(pinned_products);
		}

		private void Unpin_Click(object sender, RoutedEventArgs e)
		{
			Button src = sender as Button;

			pinned_products.Remove(pinned_products.Keys.ToList()[Grid.GetRow(src.Parent as Grid)]);

			Pin_Grid.Children.Clear();
			Pin_Grid.RowDefinitions.Clear();
			AddPinnedToGrid(pinned_products);
		}

		/// <summary>
		/// Check if the product is not already in the favourites list. Used to assign the proper
		/// button events to the pinned products.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		private bool FavouriteCheck(Product item)
		{
			if (!Directory.Exists(@"../../Users/" + username))
			{
				Directory.CreateDirectory(@"../../Users/" + username);
			}

			if (!File.Exists(@"../../Users/" + username + @"/favourites.csv"))
			{
				File.Create(@"../../Users/" + username + @"/favourites.csv").Close();
			}

			if (new FileInfo(@"../../Users/" + username + @"/favourites.csv").Length == 0)
			{
				StreamWriter streamWriter = new StreamWriter(@"../../Users/" + username + @"/favourites.csv");
				CsvWriter csvWriter = new CsvWriter(streamWriter);
				csvWriter.Configuration.Delimiter = ",";
				csvWriter.WriteHeader<Product>();
				csvWriter.NextRecord();
				streamWriter.Close();
			}

			StreamReader streamReader = new StreamReader(@"../../Users/" + username + @"/favourites.csv");
			CsvReader csvReader = new CsvReader(streamReader);
			csvReader.Configuration.Delimiter = ",";

			List<Product> userRecords = csvReader.GetRecords<Product>().ToList();
			bool alreadyFav = false;

			foreach (var record in userRecords)
			{
				if (record.Name == item.Name && record.ShopName == item.ShopName && record.Price == item.Price)
				{
					alreadyFav = true;
				}
			}

			streamReader.Close();

			return alreadyFav;
		}
	}
}
